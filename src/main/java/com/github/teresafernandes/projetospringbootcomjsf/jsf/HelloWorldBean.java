package com.github.teresafernandes.projetospringbootcomjsf.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class HelloWorldBean {
	public String getHello() {
		return hello;
	}
	
	public String hello;
	
	public void clickBotao(){
		hello = "Hello from PrimeFaces and Spring Boot1!";
	}
	
	public String clickBotaoRedireciona(){
		hello = "Hello from PrimeFaces and Spring Boot2!";
		return "index.jsf";
	}
}
